textx
numpy
scipy
pandas >=2.2.0
pint >=0.24.3
pint-pandas ==0.6.2
fireworks >=2.0.4
pyyaml
dill
ase >=3.23.0
seaborn
vre-middleware >=1.2.2
jupyter_client
ipykernel
pympler
