"""helper functions to interface fireworks"""
import os
import contextlib
from fireworks import Workflow, Firework, FWorker, Launch
from fireworks.utilities.fw_serializers import load_object
from fireworks.core.rocket_launcher import rapidfire, launch_rocket
from virtmat.middleware.resconfig import get_default_resconfig
from virtmat.language.utilities.errors import FILE_READ_EXCEPTIONS, ObjectFromFileError
from virtmat.language.utilities.mongodb import get_iso_datetime


@contextlib.contextmanager
def launchdir(tmp_dir):
    """switch to a launch directory temporarily"""
    init_dir = os.getcwd()
    os.chdir(tmp_dir)
    try:
        yield
    finally:
        os.chdir(init_dir)


def run_fireworks(lpad, fw_ids, worker_name=None, create_subdirs=False):
    """launch fireworks with the provided fw_ids"""
    fw_query = {'fw_id': {'$in': fw_ids}}
    launch_kwargs = {'strm_lvl': lpad.strm_lvl}
    launch_kwargs['fworker'] = FWorker(name=worker_name, query=fw_query)
    cfg = get_default_resconfig()
    if cfg:
        if worker_name:
            wcfg = next(w for w in cfg.workers if w.name == worker_name)
        else:
            wcfg = cfg.default_worker
        default_launchdir = wcfg.default_launchdir or os.getcwd()
    else:
        default_launchdir = os.getcwd()
    with launchdir(default_launchdir):
        if create_subdirs:
            rapidfire(lpad, nlaunches=0, local_redirect=True, **launch_kwargs)
        else:
            for _ in fw_ids:
                launch_rocket(lpad, **launch_kwargs)


def get_nodes_providing(lpad, uuid, name):
    """find all nodes providing an output name in a workflow with given uuid"""
    return lpad.get_fw_ids_in_wfs({'metadata.uuid': uuid}, {'spec._tasks.outputs': name})


def get_root_node_ids(lpad, uuid):
    """find the special root node in a workflow with given uuid"""
    return lpad.get_fw_ids_in_wfs({'metadata.uuid': uuid}, {'name': '_fw_root_node'})


def get_parent_nodes(lpad, uuid, firework):
    """find parent nodes for a new firework in a workflow with a given uuid"""
    if all('inputs' in t and len(t['inputs']) == 0 for t in firework.tasks):
        parents = get_root_node_ids(lpad, uuid)
        assert len(parents) == 1
    else:
        inputs = [i for t in firework.tasks for i in t['inputs']]
        parents = set()
        for inp in inputs:
            nodes = get_nodes_providing(lpad, uuid, inp)
            if len(nodes) != 0:
                assert len(nodes) == 1
                parents.update(nodes)
            elif inp in firework.spec:
                parents.add('root_node')
            else:
                parents.add(None)
        assert len(parents) > 0
        if 'root_node' in parents:
            parents.remove('root_node')
            if len(parents) == 0:
                parents = get_root_node_ids(lpad, uuid)
                assert len(parents) == 1
    return list(parents)


def get_ancestors(lpad, fw_id):
    """return a list of all ancestors' fw_ids of a node with fw_id"""
    wfl = get_nodes_info(lpad, {'nodes': fw_id}, {}, {'_id': True}, {'links': True})
    wfl_pl = Workflow.Links.from_dict(wfl[0]['links']).parent_links
    parents = wfl_pl.get(fw_id, [])
    ancestors = set()
    while parents:
        ancestors.update(parents)
        new_parents = set()
        for par in iter(parents):
            new_parents.update(wfl_pl.get(par, []))
        parents = new_parents
    return list(ancestors)


def safe_update(lpad, fw_id, update_dict):
    """update the node spec in a safe way"""
    state = lpad.fireworks.find_one({'fw_id': fw_id}, {'state': True})['state']
    if state == 'COMPLETED':
        lpad.defuse_fw(fw_id, rerun_duplicates=False)
        lpad.update_spec([fw_id], update_dict)
        lpad.reignite_fw(fw_id)
    elif state in ['WAITING', 'READY']:
        lpad.update_spec([fw_id], update_dict)
    elif state == 'FIZZLED':
        lpad.update_spec([fw_id], update_dict)
        lpad.rerun_fw(fw_id, rerun_duplicates=False)


def get_nodes_info_mongodb(lpad, wf_query, fw_query, fw_proj, wf_proj=None):
    """find all nodes for each matching workflow and returns selected info"""
    wfp = {'metadata.uuid': True} if wf_proj is None else dict(wf_proj)
    fw_pipeline = [{'$match': {**fw_query, '$expr': {'$in': ['$fw_id', '$$mynodes']}}},
                   {'$project': {**fw_proj, '_id': False}}]
    wf_lookup = {'from': 'fireworks', 'let': {'mynodes': '$nodes'},
                 'pipeline': fw_pipeline, 'as': 'nodes'}
    wf_pipeline = [{'$match': wf_query}, {'$lookup': wf_lookup},
                   {'$project': {'_id': False, 'nodes': True, **wfp}}]
    cursor = lpad.workflows.aggregate(wf_pipeline)
    return list(cursor)


def get_nodes_info_mongomock(lpad, wf_query, fw_query, fw_proj, wf_proj=None):
    """find all nodes for each matching workflow and returns selected info"""
    wfp = {'metadata.uuid': True} if wf_proj is None else dict(wf_proj)
    wfp.update({'nodes': True, '_id': False})
    wfs = []
    for wfn in lpad.workflows.find(wf_query, wfp):
        fwq = {**fw_query, '$expr': {'$in': ['$fw_id', wfn['nodes']]}}
        wfn['nodes'] = list(lpad.fireworks.find(fwq, fw_proj))
        wfs.append(wfn)
    return wfs


if os.environ.get('MONGOMOCK_SERVERSTORE_FILE'):
    get_nodes_info = get_nodes_info_mongomock
else:
    get_nodes_info = get_nodes_info_mongodb


def object_from_file(class_, path):
    """a wrapper function to call various from_file-type class methods"""
    try:
        return class_.from_file(path)
    except tuple(FILE_READ_EXCEPTIONS) as err:  # tuple() to avoid E0712
        raise ObjectFromFileError(path) from err


def retrieve_value(lpad, launch_id, name):
    """retrieve the output value from the FWAction of a given launch"""
    return lpad.get_launch_by_id(launch_id).action.update_spec[name].value


def get_vary_df(lpad, uuid):
    """retrieve the vary table from the meta-node"""
    wf_q = {'metadata.uuid': uuid}
    fw_q = {'name': '_fw_meta_node', 'spec._vary': {'$exists': True}}
    fw_p = {'spec._vary': True}
    nodes = next(d['nodes'] for d in get_nodes_info(lpad, wf_q, fw_q, fw_p))
    if nodes:
        assert len(nodes) == 1
        return load_object(nodes[0]['spec']['_vary'])
    return None


def get_representative_launch(launches):
    """
    Select a representative launch (one with the largest state rank) from a list.
    If there are multiple COMPLETED launches then the most recent of them is returned.
    This function is almost identical to the internal static method with the same
    name in fireworks/core/firework.py

    Args:
        launches ([Launch]): iterable of Launch objects

    Returns:
        (Launch): a representative launch
    """
    max_score = Firework.STATE_RANKS['ARCHIVED']
    m_launch = None
    completed_launches = []
    for launch in launches:
        if Firework.STATE_RANKS[launch.state] > max_score:
            max_score = Firework.STATE_RANKS[launch.state]
            m_launch = launch
            if launch.state == 'COMPLETED':
                completed_launches.append(launch)
    if completed_launches:
        return max(completed_launches, key=lambda v: v.time_end)
    return m_launch


def get_fw_metadata(lpad, wf_query, fw_query):
    """retrieve fireworks metadata"""
    proj_keys = ['fw_id', 'name', 'state', 'created_on', 'updated_on',
                 'launches', 'archived_launches', 'spec._category',
                 'spec._worker', 'spec._grammar_version', 'spec._python_version',
                 'spec._data_schema_version', 'spec._dupefinder']
    wfs = get_nodes_info(lpad, wf_query, fw_query, {k: True for k in proj_keys},
                         {'metadata': True, 'parent_links': True})
    assert len(wfs) == 1
    assert len(wfs[0]['nodes']) == 1
    node = wfs[0]['nodes'][0]
    launch_q = {'launch_id': {'$in': node['launches']}}
    launch_proj = {'action': False, 'trackers': False}  # avoid bulky objects
    launches = [Launch.from_dict(d) for d in lpad.launches.find(launch_q, launch_proj)]
    launch_q = {'launch_id': {'$in': node['archived_launches']}}
    arch_launches = [Launch.from_dict(d) for d in lpad.launches.find(launch_q, launch_proj)]
    total_runtime = (sum(i.runtime_secs for i in launches) +
                     sum(i.runtime_secs for i in arch_launches))
    return {'group UUID': wfs[0]['metadata']['g_uuid'],
            'model UUID': wfs[0]['metadata']['uuid'],
            'node UUID': node['name'],
            'node ID': node['fw_id'],
            'parent IDs': wfs[0]['parent_links'].get(str(node['fw_id']), []),
            'node state': node['state'],
            'created on': get_iso_datetime(node['created_on']),
            'updated on': get_iso_datetime(node['updated_on']),
            'grammar version': node['spec'].get('_grammar_version'),
            'data schema version': node['spec'].get('_data_schema_version'),
            'python version': node['spec'].get('_python_version'),
            'category': node['spec'].get('_category'),
            'fworker': node['spec'].get('_worker'),
            'dupefinder': bool(node['spec'].get('_dupefinder')),
            'reservation ID': lpad.get_reservation_id_from_fw_id(node['fw_id']),
            'number of launches': len(node['launches']),
            'number of archived launches': len(node['archived_launches']),
            'launch_dir': [lnch.launch_dir for lnch in launches],
            'archived launch_dir': [lnch.launch_dir for lnch in arch_launches],
            'runtime_secs': get_representative_launch(launches).runtime_secs,
            'runtime_secs total': total_runtime,
            }
