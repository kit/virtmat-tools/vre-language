
"""A Jupyter kernel for the virtmat language"""

__version__ = '0.1'

from .kernel import VMKernel
