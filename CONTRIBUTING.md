Everyone is welcome to contribute to the project. 

There are two main ways for contributions:
1. [Submit issues](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/new)
2. Make changes in the repository

To contribute with changes in the repository, i.e. to change the software, documentation, configuration, etc., one should follow these steps:

* Fork the project in gitlab and clone the repository of your new project locally
* Set the repository of this project as `upstream` remote
* Set your forked project repository as `origin` remote
* Keep the `master` branch of the forked project repository unchanged
* Create a new branch in the forked project repository and commit all changes to it
* [Create a merge request](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/merge_requests/new) with the upstream/master branch as target
  - If the changes are complete: indicate in the merge request title its type: enhancement (ENH), bug fix (BUG), documentation (DOC) etc.
  - If the changes are not complete: tag the merge request title with WIP (work in progress) or select the draft status.
  - Describe the changes and the motivation for the changes in a couple of sentences. If the merge request is solving an open issue, then the issue should be specified with a link.
* The branch will be merged after the following conditions are fulfilled:
  - the continuous integration (CI) pipeline has run with no errors;
  - the draft status is deactivated (or WIP tag removed);
  - all threads opened by the maintainer approving the merge request are resolved;
  - at least one maintainer approves the merge request.
