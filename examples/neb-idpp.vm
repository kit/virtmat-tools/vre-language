"""
This example reproduces the following tutorial:
https://wiki.fysik.dtu.dk/ase/tutorials/neb/idpp.html
"""

initial = Structure from file '/home/ubuntu/work/vre-language/examples/ethane-i.xyz'
final = Structure from file '/home/ubuntu/work/vre-language/examples/ethane-f.xyz'

calc = Calculator emt (), task: single point

algo_if = Algorithm FIRE ((trajectory: true), (fmax: 0.05) [eV/angstrom])

prop_i = Property energy, forces ((structure: initial), (calculator: calc), (algorithm: algo_if))
prop_f = Property energy, forces ((structure: final), (calculator: calc), (algorithm: algo_if))

i_opt = prop_i.output_structure
f_opt = prop_f.output_structure
print(i_opt)
print(f_opt)
print(prop_i.energy)
print(prop_f.energy)
print(prop_i.forces)
print(prop_f.forces)

algo_rmsd = Algorithm RMSD ((reference: initial))
prop_rmsd = Property rmsd ((structure: i_opt), (algorithm: algo_rmsd))
print(prop_rmsd.rmsd)

if_neb = Structure neb_struct ((atoms: i_opt.atoms[0], f_opt.atoms[0]),
                               (cell: i_opt.cell[0], f_opt.cell[0]))

neb = Algorithm NEB, many_to_one (
                          (number_of_images: 11),
                          (interpolate_method: 'idpp'),
                          (optimizer: ((name: 'FIRE'))),
                          (filename: 'neb_ethane_plot.png'),
                          (logfile: true))

prop_neb = Property activation_energy, reaction_energy, trajectory, maximum_force
              ((structure: if_neb), (calculator: calc), (algorithm: neb))
# print(prop_neb)
print(prop_neb.activation_energy)
print(prop_neb.reaction_energy)
print(prop_neb.maximum_force)
# print(prop_neb.trajectory)
