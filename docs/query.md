# Tagging and finding models

In [workflow evaluation mode](tools.md#workflow-mode) one can process one or multiple models that belong to the active group. *Tagging* is used to be able to *find* other models that are not in the active group.

## The `tag` statement

The `tag` statement adds [tags](https://en.wikipedia.org/wiki/Tag_(metadata)) to the model that can be used in searches to identify the model. The `tag` statement is interpreted in workflow evaluation mode in both `texts script` and `texts session` tools, and in Jupyter.

### Syntax

The tag is a [table](scl.md#table) limitted to one row or a [dictionary](scl.md#dict):

```
tag ((label: 'my_model_xyz'), (keywords: ('feature1', 'feature2')))
```

The same tag can be written using [dictionary syntax](scl.md#dict) which is more compact:

```
tag {label: 'my_model_xyz', keywords: ('feature1', 'feature2')}
```


### Example:

Let us have a model with the following metadata:

```yaml
keywords:
- monolayer
- bifunctional mechanism
active site: M5
doping: Fe
environment: explicit water
mechanism: associative
intercalation: KOH
scheme: S3
constrained spin: false
formula: 2(2(KMnO2).2(MnO2.KOH))
supercell: [2, 2]
```

The corresponding tag statement will be:

```
tag {keywords: ('monolayer', 'bifunctional mechanism'),
     'active site': 'M5',
     doping: 'Fe',
     environment: 'explicit water',
     mechanism: 'associative',
     intercalation: 'KOH',
     scheme: 'S3',
     'constrained spin': false,
     formula: '2(2(KMnO2).2(MnO2.KOH))',
     supercell: (2, 2)}
```

The tag statement can be split into several smaller statements that can be added at any time in any part of the input:

```
tag {keywords: ('monolayer', 'bifunctional mechanism'),
     'active site': 'M5',
     doping: 'Fe',
     environment: 'explicit water'}
...
tag {mechanism: 'associative',
     intercalation: 'KOH',
     scheme: 'S3',
     'constrained spin': false,
     formula: '2(2(KMnO2).2(MnO2.KOH))',
     supercell: (2, 2)}
```

The tag can be updated at any time, for example, to correct the `scheme` and add `oer` to the keywords:

```
tag {scheme: 'S2'}
tag {keywords: ('monolayer', 'bifunctional mechanism', 'oer')}
```

A tag cannot be removed but can be nullified by updating it with the `null` value:

```
tag {scheme: null}
```

The tag of the active model group can be printed in `texts session` using the `%tag` [magic command](tools.md#specific-features).

## The `%find` command

With the `%find` [magic command](tools.md#specific-features) one can perform a search and an action on the matching models. It has the following syntax:

```
%find <query> [action]
```

The query is, similar to the tag parameter, a table or a dictionary. The action is optional. The default action is to list the matching models. Further actions are:

* `load one`: load the first matching model if the current active model is not in the list of matching models

The query has three sections that can be secified with the top-level keys `tags`, `meta` and `data`:

```
%find {tags: {...}, meta: {...}, data: {...}}
```

At least one key of the top-level keys must be specified. The `tags` section refers to the tags added using [`tag` statements](#the-tag-statement). The `meta` section refers to metadata that have fixed keys, such as `state`, `created_on`, `updated_on`, `metadata`, `name`, `nodes`, `links`. Similar to the tags, these are attributes of the whole model. The `data` section refers to attributes of the individual model nodes (statements), for example the variable names.

To search within the tag metadata or within the data section, the relevant keys should be prefixed with a tilde `~`.

### Examples

```
%find {tags: {'~scheme': {'$in': ('S1', 'S3')}}}
```

This will list all models tagged with scheme equal to either S1 and S3. To switch to the first matching model:

```
%find {tags: {'~scheme': {'$in': ('S1', 'S3')}}} load one
```

To search in the `meta` section:

```
%find {meta: {state: 'COMPLETED'}, tags: {'~scheme': {'$in': ('S1', 'S3')}}}
```

This will limit the search results to only those models that have been completed.