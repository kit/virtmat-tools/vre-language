# Installation

## Python

No knowledge in Python is required to use VRE Language. Nevertheless, Python is the language of the interpreter and the employed framework. Thus the typical tools for setting up Python packages are used for the installation.

To ensure support for all required packages and dependencies, currently Python version 3.9 or higher is required.

## Installation using `pip`

The project can be installed into a Python virtual environment using the `pip` package manager. This is achieved in the following steps:

1. Create a Python virtual environment (replace `/path/to` with an existing path and `venv-name` with a new name):

    ```
    python -m venv /path/to/venv-name
    ```

1. Activate your Python virtual environment, e.g. with

    ```
    source /path/to/venv-name/bin/activate
    ```

1. Make sure you have the latest version of pip for your python installation:

    ```
    python -m pip install --upgrade pip
    ```

1. Install the VRE Language package and its dependencies into the environment:

    ```
    python -m pip install vre-language
    ```

**NOTE: Warnings due to an open issue in FireWorks**

Fresh installations of VRE Language will currently *sometimes* print many warnings containing this line when running `texts`:

```
ModuleNotFoundError: No module named 'pytest'
```

The issue is documented [here](https://github.com/materialsproject/fireworks/issues/535).

The warning can be ignored or muted by installing the `pytest` package via this command:

```
python -m pip install pytest
```

## Installation in developer (editable) mode

For developing purposes, VRE Language can be installed in editable mode in which changes will be tracked in a branch of a local Git repository and can be immediately tested without need of re-install or update the package.

###  Install from a custom local Git repository

1. The VRE Language Git repository must be cloned with the command (skip if you have a local repository):

```
git clone https://gitlab.kit.edu/kit/virtmat-tools/vre-language
```

Alternatively, a specific commit or tag can be downloaded directly from the [project's  GitLab webpage](https://gitlab.kit.edu/kit/virtmat-tools/vre-language) as an archive and unpacked locally. 


2. After switching to the top-level folder `vre-language` of the local repository, install with `pip`:

```
python -m pip install -e .[test]
```

**NOTE**: The extra `[test]` added to the name of the package installs a set of additional packages for testing the installed package.

### Install directly from the remote Git repository

To install from the remote Git repository directly one can use this command:

```
python -m pip install -e git+https://gitlab.kit.edu/kit/virtmat-tools/vre-language.git#egg=vre-language[test]
```

In this case a local clone of the Git repository is created under `/path/to/venv-name/src/vre-language` where the code can be managed.

## Setting up FireWorks 

The VRE Language uses the FireWorks workflow management system to persist data and metadata. For production purposes FireWorks has to be configured with a MongoDB database. A short guide how to do this can be found [here](https://vre-middleware.readthedocs.io/en/latest/installation.html#setting-up-fireworks-with-a-mongodb-instance).