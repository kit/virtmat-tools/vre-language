VRE Language
============

What is VRE Language?
---------------------

The *VRE Language* package includes a family of `domain-specific languages (DSLs)  <https://en.wikipedia.org/wiki/Domain-specific_language>`_. 
Originally it has been developed to be used in a `Virtual Research Environment (VRE) <https://en.wikipedia.org/wiki/Virtual_research_environment>`_
in `VirtMat projects <https://www.materials.kit.edu/virtmat_current_projects.php>`_.

Currently, the VRE Language family includes two members:

* textS: scientific computing language (SCL) - for any domain of computational science
* textM: atomic and molecular modeling language (AMML) - for the domain of computational materials science

In addition, the VRE Language package includes all tools you need to use textS and textM - parsers, interpreters, interfaces to backend systems etc., as well as a Jupyter kernel that allows writing Jupyter notebooks in these languages.

What is a domain-specific language?
-----------------------------------

This is a computer language using domain-specific notations and abstractions different from those commonly used in
general purpose languages, such as Python. For example, in VRE Language there are no for-loops and classes
used in Python, and no workflows and workflow node objects used in workflow description languages. Instead, 
VRE Language contains objects from the domain and semantics to perform specific operations on these. For example, textM 
has objects such as atomic structures, calculators, etc. similar to those in the `Atomic Simulation Environment (ASE) <https://wiki.fysik.dtu.dk/ase/>`_
but these objects and the operations on them are part of textM whereas in ASE they are not part of Python.

Is VRE Language for me?
-----------------------

You may want to use VRE Language if you:

* have complex models that require coupling several different codes in several Slurm jobs, or include multiple different steps with the same code as Slurm jobs.
* perform high throughput computing (i.e. many runs of the same type), especially in cases with multiple computing and analysis steps.
* already use a `workflow management system <https://github.com/meirwah/awesome-workflow-engines>`_ but want to save time and effort for development and extensions and increase the reuse of your data.

Have a look at the `use cases <https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/tree/master/examples>`_ to get a first impression:

* virtual screening of catalysts for the oxygen reduction and oxygen evolution reactions.
* typical classical molecular dynamics simulation - setup, equilibration, sampling and analysis steps.


When should I **not use** VRE Language?
---------------------------------------

VRE Language is thought to link computational jobs and their data and to cover pre- and post-processing. In contrast to the most workflow systems,
VRE Language allows any operations involving numeric and boolean expressions, series, arrays and tables (data frames). As long as these are implemented in Numpy and Pandas these can be used in HPC jobs.
However, VRE Language is not thought to replace C++ or Fortran in developing high-performance computing code. Instead, VRE Language should be used to link 
such codes without having the troubles with resource management and workflow management systems, and tracking input and output files.


.. toctree::
   :maxdepth: 1
   :caption: Quick start

   quickstart
   installation

.. toctree::
   :maxdepth: 1
   :caption: Basic level

   scl
   amml
   chemistry
   tools
   jupyter

.. toctree::
   :maxdepth: 1
   :caption: Advanced level

   resources
   io
   bulk
   query
   submodel
   extending_scl

.. toctree::
   :maxdepth: 1
   :caption: Help

   faqs
   support

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
