# Jupyter kernel for the SCL and AMML languages


## Installation

After installing the `vre-language` package according to the [installation guide](installation.md), run this command to install the kernel:


```bash
python -m virtmat.language.kernel.install
```

To check if the installation has been successful run the command

```bash
jupyter kernelspec list
```

to see all installed Jupyter kernels. The new kernel should appear there under the name `texts`. Then, after opening a Jupyter Notebook, a new kernel with the name `textS/textM` should appear in the drop-down menu on the top right. In Jupyter Lab, the kernel appears in the Launcher tab.


## Short overview of the implementation

The current implementation is based on the [jupyter_client documentation](https://jupyter-client.readthedocs.io/en/stable/kernels.html) and includes the `VMKernel` class which is derived from the 
`Kernel` class in module `ipykernel.kernelbase`. It further supports auto-completion
(by pressing the tab key) of variable and function names that it knows from 
previous cells that have already been run. The keywords `print` and `use` are always 
auto-completed, even in the very first cell.

The `Kernel` class of `ipykernel.kernelbase` 
already has an attribute called `session`. This should not be confused with the `Session` class in `vre-language` package. The attribute `vmlang_session` in the kernel is an object of this
`Session` class.

The `VMKernel` class has two principal methods, `do_execute` and `do_complete`. The `do_execute` method is the heart of the kernel and handles the messaging protocols. It takes the input ("code") and creates a textX model from it. Then, the model value ("output") is passed as value belonging to the key "text" in the 
dictionary "stream_content" and sent to the "iopub_socket" to show up as printed 
output in the notebook.

The `do_complete` method only concerns the auto-completion functionality of the kernel.
The kernel is fully functional also without the `do_complete` method. The
method accesses the list `self.memory` which is initialized to contain "print"
and "use". While cells are executed, the namespace grows, and the names (functions, variables, and imports) are
added to `self.memory`. Messaging again
works with dictionaries of very definite structure and naming.
