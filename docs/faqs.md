# Frequently asked questions (FAQs)


**Q**: I am getting the following error: `pymongo.errors.DocumentTooLarge: 'findAndModify' command document too large`. What can I do?

**A**: You can change the default setup for [background I/O](io.md#background-io-operations).


**Q**: I cannot find a model in the database, what can I do?

**A**: You can use [tags](query.md#the-tag-statement) and then [search](query.md#the-find-command) the database for known tags, metadata and data.


**Q**: The statements in the model are evaluated in an order that is non-intuitive. I wish certain statements to be evaluated now and other statements - only later.

**A**: You can consider using the `--on-demand` option to either `texts session` or `texts script`. Read more [here](tools.md#supporting-tools).


**Q**: Slurm does not accept batch jobs with `--account` but `texts` adds my username as the default account.

**A**: Open an `ipython` session and run the following code:
```python
from virtmat.middleware.resconfig import get_default_resconfig, get_resconfig_loc
cfg = get_default_resconfig()
cfg.default_worker.accounts.append(None)
cfg.default_worker.default_account = None
cfg.to_file(get_resconfig_loc())
```
Then run `texts` again. Then use the [variable update](scl.md#dealing-with-failures) (using `:=`) to inforce re-evaluation of the resources. A simple `%rerun var` will not be sufficient.

**Q**: How can I set a datastore configuration alternative to the default `$HOME/.fireworks/datastore_config.yaml`?

**A**: This can be done using the [vre-middleware's](https://vre-middleware.readthedocs.io/en/latest/resconfig.html) Python API. For example, within an iPython command line session:
```python
from virtmat.middleware.resconfig import get_resconfig_loc, get_default_resconfig
cfg = get_default_resconfig()
wcfg = cfg.workers[0] # SEE NOTE
wcfg.envvars.update({'DATASTORE_CONFIG': '/alternative/path_to/my_project/datastore_config.yaml'})
wcfg.default_envvars.append('DATASTORE_CONFIG')
cfg.default_worker = wcfg
cfg.to_file(get_resconfig_loc())
```

**NOTE:** In this example `worker[0]` is assumed as the relevant worker for the datastore configuration. If available, other workers can be selected by using the corresponding index, eg `worker[n]`.