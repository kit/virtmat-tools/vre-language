# Sub-model reuse

Sometimes it is necessary to combine data from more than one model in a new model. Especially, data that has been produced with large computational effort should be reused without the need to recompute. To illustrate this use case, let us consider a very simple model

```
a = 1
```

Supposed, we have another model 

```
b = 2
```

and want to add `a` from the first model to `b`. Of course, if `a` is just a simple number there is no much benefit of reusing it but if `a` is the output of a complex sub-model within the first model and the computations have taken a lot of resourcest, then this reuse is desirable. On the other hand, it may happen that `b` has also been produced with effort. In other words, one would want to combine the two models into one new model.

In the workflow evaluation mode, this can be realized using a reference to `a` from the first (source) model with UUID `<model_uuid>`, thus the second (target) model becomes:

```
b = 2
c = b + a@<model_uuid>
```

Now, not only `c` has been computed but also `a` has been copied to the target model.

## Current limitations

### Names, scoping and name spaces

All names in the scope of the target model may not collide with any names reused from the source model. This is non-trivial to check by the user but this check is performed by the interpreter and it make no changes if a name conflict has been found.

In addition, there may be no name conflicts between the names of variables reused from multiple source models. For example, this reuse statement will not work:

```
b = a@<model_uuid_1> + a@<model_uuid_2>
```

This limitation has several further implications. For example, one may not reuse a variable in the source model that depends on variables that have been already reused in the target model.

In future, the `<model_uuid>` will be used to extend the names in the local scope or even to construct a namespace.

### Internal functions and imports

Currently, reuse of internal functions and imports from other models is not limited to referenced objects. Rather all internal functions and object imports defined in the source model are copied to the target model, no matter whether they are referenced by the reused variable.
