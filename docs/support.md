# Support contact

If you need support or have any questions about the VRE Language package please send a message to the [virtmat-tools distribution list](mailto:virtmat-tools@lists.kit.edu).
