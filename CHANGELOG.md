# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/)

## [Unreleased]

## [0.5.6] - 2025-02-05

### Added
- **docs**: add a section on resource configuration ([#411](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/411))
- **amml**: apply constraints optionally in view structure ([#405](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/405))
- **amml**: add periodic boundary conditions (PBC) to input structure automatically with Vasp calculator ([#401](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/401))

### Fixed
- **interpreter**: fix warnings not being suppressed by `-w` flag ([#412](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/412))
- **cli**: fix cases with empty search results to avoid crash of `%find ... load one` ([#410](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/410))
- **interpreter**: fix printing the type in case of evaluation error ([#408](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/408))
- **grammar**: fix a semantic version grammar rule ([#404](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/404))

### Changed
- **setup**: adapt package dependencies for the new fireworks release ([#413](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/413))
- **amml**: change default value of an environment variable to the vendor default ([#411](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/411))
- **interpreter**: do not show Python exception traceback of evaluation errors ([#402](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/402))
- **docs**: make several corrections in docs ([#400](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/400))
- **grammar**: make parse rule version requirement more precise ([#354](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/354))

## [0.5.5] - 2025-01-13

### Added
- **docs**: add FAQ to docs ([#392](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/392))
- **docs**: add a link in the docs regarding FireWorks setup ([#387](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/387))
- **amml**: implement velocity distributions ([#385](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/385))
- **interpreter**: add support for tuple subscripting ([#379](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/379))
- **amml**: enable plotting reaction energy profile diagrams ([#371](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/371))

### Fixed
- **interpreter**: infer the subdtype but not convert to default subdtype with pint-pandas >=0.7 ([#396](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/396), [#398](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/398))
- **interpreter**: add end-of-file to terminated general reference rule to allow variable with a single reference as parameter ([#389](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/389))
- **interpreter**: fix diverse type errors causing crashes ([#394](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/394))
- **interpreter**: prevent crash when printing function value in asynchronous on-demand evaluation ([#391](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/391))
- **grammar**: prevent crash with numeric literal in place of units ([#388](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/388))
- **interpreter**: handle exceptions from database with large documents (solved in the context of [#388]((https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/388)))
- **cli**: prevent crash in the import section of text session ([#384](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/384))
- **constraints**: check first argument of issubclass ([#382](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/382))
- **interpreter**: prevent crash with type() function applied to imports ([#386](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/386))
- **examples**: update the examples for the recent grammar changes ([#380](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/380))
- **interpreter**: prevent evaluating non-active models in case of synchronous and asynchronous on-demand workflow evaluation ([#381](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/381))
- **docs**: correct the docs section about extending SCL ([#378](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/378))

### Changed
- **cli**: revise some CLI options: remove `--textx-debug`, add `%help` in `%help` menu; produce warnings for ignored parameters ([#397](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/397))
- **docs**: adjust the docs for how to use the wfengine file dumped by `texts session` ([#397](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/397))
- **amml**: adapt calculator parameters due to task ([#274](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/274))
- **grammar**: remove variable tuple from grammar, interpreter, examples, tests, and docs ([#379](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/379))
- **cli**: sort model history by time stamps ([#368](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/368))


## [0.5.4] - 2024-11-28
### Added
- **cli**: add features to interactive console session ([#372](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/tree/372-add-features-to-interactive-console-session))
- **amml**: implement the Vibrations algorithm ([#363](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/363))

### Fixed
- **interpreter**: configure custom qadapter with a custom launchpad file ([#374](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/374))
- **interpreter**: prevent processing view statements of all models in a model group ([#373](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/373))
- **interpreter**: view statement does not trigger evaluation on demand ([#369](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/369))
- **constraints**: type crashes if the parameter is None (unknown) type ([#367](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/367))
- **interpreter**: non-handled exception when the units in view statement in wrong order ([#366](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/366))

### Changed
- **cli**: use the code and readline modules for the interactive session ([#241](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/241))


## [0.5.3] - 2024-11-15
### Added
- **utilities**: add a data schema version compatibility layer ([#357](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/357))
- **logos**: add logos for textS and textM ([#353](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/353))
- **docs**: add support contact ([#346](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/346))
- **amml**: implement NEB and Dimer algorithms ([#302](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/302))

### Fixed
- **utilities**: prevent use of tkinter by matplotlib when graphics are not displayed ([#362](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/362))
- **amml**: allow calculators to use defaults when parameters lists are empty ([#360](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/360))
- **metamodel**: fix setting and checking computing resources ([#359](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/359))
- **metamodel**: enable update of resource annotations ([#358](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/358))
- **interpreter**: allow vary with float-type variables ([#356](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/356))
- **interpreter**: check session consistency, restart if needed ([#347](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/347))
- **interpreter**: remove ambiguity in condition_comparison ([#344](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/344))
- **constraints**: fix parameter type check in calc and algo objects ([#343](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/343))
- **amml**: fix crash with argon_nvt.vm example ([#338](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/338))
- **interpreter**: fix non-handled exception updating resource annotations ([#332](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/332))

### Changed
- **docs**: minor improvements in quick-start guide ([#365](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/365))
- **interpreter**: simplify / unify internal representation of numerical series ([#350](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/350))
- **amml**: refactor get_ase_property() ([#273](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/273))
- **amml**: enable processing of AMML parameters as iterables ([#268](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/268))


## [0.5.2] - 2024-09-27
### Added
- **amml**: enable band structure property ([#335](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/335))
- **amml**: enable ASE's density of states algorithm ([#334](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/334))
- **amml**: enable ASE's equation of state algorithm ([#333](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/333))
- **utilities**: enable launchdir from resconfig for interactive nodes ([#328](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/328))
- **cli**: print upon request interpreter, grammar and schema version ([#325](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/325))
- **amml**: allow extraction of intermediate structures and magnetic moments ([#324](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/324))

### Fixed
- **setup**: pin mongomock package version to avoid crash caused by new release ([#340](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/340))
- **amml**: fix stress property for vasp calculator ([#339](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/339))
- **grammar**: fix some attribute names causing syntax errors ([#330](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/330))
- **interpreter**: avoid session crash from %find with incorrect query ([#329](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/329))
- **interpreter**: avoid crash from non-initialized database ([#327](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/327))
- **interpreter**: avoid errors with tag and %find with improper content ([#322](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/322))

### Changed
- **amml**: refactor AMML handler functions ([#337](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/337))
- **utilities**: implement type checking for method parameters ([#336](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/336))
- **setup**: create a workaround for fireworks problems in new installations ([#326](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/326))
- **docs**: update and improve documentation ([#323](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/323))


## [0.5.1] - 2024-08-23
### Fixed
- **constraints**: fix array datalen in two sets ([#320](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/320))
- **interpreter**: add checks of units of numeric series at runtime ([#319](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/319))
- **interpreter**: add a constraint processor to check units of series literals ([#317](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/317))
- **utilities**: fix formatter for pandas.NA type ([#316](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/316))
- **amml**: load atom positions and momenta into Structure using pint series ([#315](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/315))
- **grammar**: fix retrieval of Series-type parameter as array ([#314](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/314))
- **interpreter**: avoid use of variable mutation on grouped variables generated via vary method ([#312](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/312))
- **interpreter**: allow use of vary statement on existing variables ([#310](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/310))
- **grammar**: issue syntax error if import statement is missing namespace ([#307](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/307))
- **interpreter**: enable use of many_to_one without explicitly setting nbins  ([#258](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/258))

### Changed
- **docs**: adapt examples to avoid crashes from static checks ([#318](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/318))


## [0.5.0-beta2] - 2024-08-09
### Added
- **grammar**: implement grammar version checker in duplicate detector and sub-model reuse ([#282](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/282))
- **interpreter**: support optionally mutable parameters ([#260](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/260))
- **interpreter**: provide transparent file storage for large objects and datasets ([#245](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/245))
- **amml**: verify balance of chemical equations based on `Species` composition ([#231](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/231))
- **interpreter**: implement flexible metadata tag system for bulk-mode operations ([#220](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/220))
- **interpreter**: allow optional use of filepad on the database for files ([#200](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/200))
- **amml**: add aditional information to name attribute of AMML structures ([#134](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/134))

### Fixed
- **interpreter**: fix model not found due to empty uuid ([#304](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/304))
- **interpreter**: fix statement separation with semicolons in workflow mode ([#300](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/300))
- **interpreter**: allow definition of functions at end of input ([#299](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/299))
- **grammar**: permit expressions starting with references to object accessors ([#298](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/298))
- **utilities**: avoid simultaneous off-loading and in-lining of data([#296](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/296))
- **interpreter**: stop syntax error and session crash from incorrect magics ([#295](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/295))
- **interpreter**: allow empty input in interactive sessions ([#294](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/294))

### Changed
- **docs**: include documentation entries for structure-intrinsic properties ([#305](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/305))
- **cli**: remove CLI scripts not covered by tests ([#303](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/303))
- **docs**: format and homogenize math and units in the docs ([#301](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/301))
- **interpreter**: decouple UserWarning from domain-specific warnings ([#297](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/297))
- **docs**: add trajectory property to AMML documentation section ([#289](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/289))


## [0.4.0-beta1] - 2024-07-12
### Added
- **docs**: add a changelog ([#278](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/278))
- **utilities**: support FireWorks package without MongoDB ([#277](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/277))
- **amml**: support additional proprties of AMML Structure objects ([#271](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/271))
- **metamodel**: implement resource evaluation for pre_rocket feature ([#251](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/251))
- **tests**: add tests for CI tools ([#242](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/242))
- **interpreter**: provide a switch to deactivate duplicate detection ([#237](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/237))
- **metamodel**: allow dynamic worker name according to resources ([#238](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/238))
- **amml**: add properties key to species and reaction parameters ([#232](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/232))
- **metamodel**: allow flexible selection of batch queue ([#228](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/228))
- **kernel**: add jupyter kernel to the package ([#218](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/218))
- **interpreter**: enable use of `%start` command and `--file | -f` flag in interactive session ([#203](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/203))
- **docs**: include recommendations for model execution ([#199](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/199))
- **docs**: include a quickstart guide ([#193](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/193))
- **amml**: add AMML algorithms to work with structures and calculators ([#141](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/141))
- **interpreter**: avoid optionally creation of individual `launcher_` folders ([#75](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/75))
- **metamodel**: enable graphical display of @D datasets and AMML structures ([#12](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/12))

### Fixed
- **interpreter**: enable starting a new model in a running session ([#293](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/293))
- **interpreter**: fix invalid UUID and session crash ([#292](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/292))
- **interpreter**: avoid copying unnecessary data from sub-model reuse ([#291](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/291))
- **constraints**: fix numeric and datatype inconsistencies when reusing tables ([#288](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/288))
- **cli**: improve mongomock support for FireWorks ([#284](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/284))
- **utilities**: fix formatting errors caused by pint release 0.24 ([#281](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/281))
- **utilities**: enable subscripting cell array ([#272](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/272))
- **ci**: fix CI pipeline with new container ([#270](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/270))
- **amml**: fix data retrieval for `vibrational_energies` and `energy_minimum` properties ([#269](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/269))
- **ci**:  fix import errors in pylint CI job ([#267](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/267))
- **interpreter**: allow functions with name reference ([#266](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/266))
- **utilities**: fix incorrect format of structure output in workflow mode ([#264](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/264))
- **metamodel**: fix `object_to` statement in persistent models ([#261](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/261))
- **grammar**: support expressions starting with a function call ([#257](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/257))
- **utilities**: wrap errors from operations with ofset units ([#250](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/250))
- **metamodel**: fix incorrect passing of memory resource annotation ([#247](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/247))
- **amml**: define and limit relationship between properties and calculator tasks ([#175](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/254))

### Changed
- **interpreter**: improve parsing in the session manager ([#290](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/290))
- **utilities**: optimize database queries ([#283](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/283))
- **interpreter**: add names of created FireWorks workflows ([#276](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/276))
- **interpreter**: allow errors per print statement without aborting control flow ([#256](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/256))
- **interpreter**: allow access to type and further metadata of parameters ([#255](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/255))
- **amml**: update Property syntax in TextM examples ([#253](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/253))
- **cli**: improve script and session interfaces and option flags ([#249](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/249))
- **grammar**: simplify Table syntax ([#153](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/153))


## [0.3.1] - 2024-02-05
### Added
- **docs**: add documentation of AMML chemistry section ([#224](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/224))

### Fixed
- **setup**: pin version 0.18.0 of the ruamel.yaml package ([#230](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/230))
- **setup**: upgrade required python version to `>=3.9` unpin `pint` package ([#229](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/229))
- **setup**: enable installation of `vre-middleware` from gitlab repository ([#227](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/227))
- **constraints**: fix `map` with lambda function and non-matching argument parameter ([#225](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/225))
- **interpreter**: fix sub-model reuse ([#223](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/223))
- **interpreter**: fix interpreter crash from extending persistent models ([#219](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/219))
- **constraints**: allow function call within another function using same dummy identifier ([#22](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/22))
- **ci**: fix jobs failing due to hanging child process ([#235](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/235))


## [0.3.0] - 2024-01-12
### Added
- **cli**: add scripts to the package ([#217](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/217))
- **grammar**: include grammar in the package ([#216](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/216))
- **interpreter**: improve control for log messages and log level ([#180](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/180))
- **interpreter**: provide annotations for output units ([#174](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/174))
- **utilities**: add context in TextXError when possible ([#155](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/155))
- **interpreter**: save source code of object imports and functions into root node spec ([#151](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/151))
- **utilities**: implement improved duplicate finder ([#146](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/146))
- **interpreter**: support high throughput processing ([#145](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/145))
- **interpreter**: enable sub-model reuse ([#144](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/144))
- **utilities**: implement grammar and interpreter version compatibility check ([#127](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/127))
- **utilities**: extend slices and accessors for arrays ([#120](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/120))
- **grammar**: add support for complex numbers ([#105](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/105))


### Fixed
- **interpreter**: fix use of `map` function in deferred mode ([#206](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/206))
- **interpreter**: fix workflow execution of AMML reaction objects ([#204](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/204))
- **amml**: fix value error from AMML property ([#192](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/192))
- **utilities**: fix units in multi-dimensional integer arrays ([#190](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/190))
- **utilities**: fix error from using the structure of a property slice ([#152](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/152))

### Changed
- **interpreter**: suppress comparison of booleans ([#163](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/163))
- **constraints**: improve location and text of error messages ([#160](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/160))
- **kernel**: print important error messages as cell output ([#150](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/150))
- **amml**: do not require units for multiplicity parameter in Turbomole calculator ([#212](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/212))
- **amml**: set list of array type for `vibrational_energies` property ([#211](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/211))
- **interpreter**: support use of the `reduce` function within another function in deferred mode ([#207](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/207))
- **interpreter**: enable use of series of numerical arrays ([#196](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/196))
- **interpreter**: allow independent use of `-a` and `-r` flags ([#191](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/191))
- **utilities**: add `rocket_launch` key to qadapter of background wrkflow execution ([#189](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/189))
- **interpreter**: force computing host to use resource annotations in firework ([#181](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/181))
- **interpreter**: allow repeated initialization of variables ([#89](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/89))


## [0.2.0] - 2023-08-25
### Added
- **amml**: add more explicit keywords in `property` ([#173](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/173))
- **amml**: implement additional properties required for oxycat use case ([#171](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/171))
- **utilities**: implement chemical species and chemical reaction rules ([#143](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/143))
- **amml**: allow use of calculators without parameters ([#133](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/133))
- **amml**: support for geometric constraints ([#113](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/113))
- **amml**: access properties and queries of AMML objects ([#112](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/112))
- **utilities**: implement mapping of DSL functions to Python API functions ([#96](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/96))
- **amml**: implement Atoms data type ([#10](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/10))
- **docs**: add documentation for the common parts of the scientific computing language ([#162](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/162))
- **constraints**: allow non-literal series objects in tables ([#124](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/124))
- **interpreter**: add support for computing resources and granularity ([#93](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/work_items/93))
- **interpreter**: add resource annotations, constrains and interpreter ([#92](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/work_items/92))
- **scripts**: make main scripts consistent and add textx debug option ([#77](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/77))

### Fixed
- **amml**: wrap error message from incorrect parameter units units in VASP calculator ([#170](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/170))
- **amml**: import caulculator from file into Property object ([#168](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/168))
- **amml**: use structures from files in Property ([#164](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/164))
- **constraints**: trigger error when using incomplete list of AMML atoms ([#161](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/161))
- **constraints**: allow use of an empty AMML calculator ([#159](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/159))
- **constraints**: detect infinite recursion in workflow evaluation ([#156](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/156))
- **scripts**: allow repeated writing objects to file in instant and deferred modes ([#136](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/136))
- **kernel**: fix VirtMat kernel installation ([#132](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/132))
- **constraints**: support function expressions and sum of non-numerical arguments in `sum` ([#130](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/130))
- **utilities**: support None datatype and `__new__` method for subclass instances ([#109](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/109))
- **setup**: resolve `dill` package as dependency ([#88](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/88))
- **kernel**: allow auto complete function to work anywhere in a cell in Jupyter kernel ([#82](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/82))
- **setup**: do not ignore python version when `pip` installing with extras ([#81](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/81))
- **setup**: support common namespace between vre-language and vre-middleware packages ([#74](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/74))
- **setup**: complete list of packages in `requirement.txt` and `setup.cfg` ([#65](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/65))
- **interpreter**: allow model extension with no variable reference via root node ([#62](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/62))
- **constraints**: correct Table references in functions on iterables ([#27](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/27))
- **metamodel**: support function definitions containing function calls([#20](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/20))


### Changed
- **grammar**: expand array data structure ([#104](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/104))
- **amml**: verify whether constraints clash with calculation specifications ([#131](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/131))
- **amml**: use `output_structure` syntax to refer to output structures ([#172](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/172))


## [0.1.0] - 2023-01-19
### Added
- **interpreter**: implement asynchronous model execution ([#72](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/72))
- **interpreter**: implement on-demand evaluation policy for remote deferred evaluation ([#69](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/69))
- **interpreter**: extend model with no variable reference ([#62](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/62))
- **grammar**: implement power expression ([#61](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/61))
- **utilities**: implement standard formatters for domain-specific output ([#59](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/59))
- **interpreter**: resolve order of I/O operations in deferred mode evaluation ([#51](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/51))
- **interpreter**: enable incremental development/dynamic models ([#46](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/46))
- **tests**: add working tests for all three modes of execution ([#45](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/45))
- **interpreter**: implement logical AND, OR, NOT. Implement bitwise AND, OR. Implement bitwise inversion ([#41](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/41))
- **grammar**: allow imported external functions in Filter, Map and Reduce ([#38](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/38))
- **ci**: add CI workflow for regression tests using [NHR infrastructure](https://www.nhr.kit.edu/userdocs/ci/) ([#31](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/31))
- **interpreter**: enable different evaluation and input/output modes ([#23](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/23))
- **grammar**: use explicit types to construct objects from scratch or to/from external source ([#21](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/21))
- **constraints**: implement cyclic dependency checker ([#15](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/15))
- **utilities**: provide module to produce domain-specific error messages ([#14](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/14))
- **constraints**: implement all constraints as model/object processors ([#13](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/13))
- **utilities**: add data query idioms ([#9](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/9))
- **grammar**: use intrinsic functions for data structures and expressions with data structures ([#8](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/8))
- **grammar**: implement data structures of type iterable (series), sequence (tuple), and sequence of iterables (table) ([#7](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/7))
- **constraints**: support for physical units for numerical types ([#5](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/5))
- **grammar**: implement definitions of internal functions ([#4](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/4))
- **grammar**: implement function calls and import of external functions ([#3](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/3))
- **constraints**: implement typing system ([#2](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/2))
- **interpreter**: create a session manager for interactive, text-based sessions ([#56](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/82))
- **kernel**: write a jupyter kernel for the new DSL ([#33](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/33))

### Fixed
- **interpreter**: display error message of failed evaluations in workflow mode ([#70](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/70))

### Changed
- **grammar**: simplify Table creation with attribute rule ([#40](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/40))
- **grammar**: use one attribute in Map, Filter, and Reduce functions ([#39](https://gitlab.kit.edu/kit/virtmat-tools/vre-language/-/issues/39))
