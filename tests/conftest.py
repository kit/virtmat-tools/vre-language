"""shared fixtures requested by tests in several different modules"""
import os
import pytest
from fireworks import LaunchPad
from fireworks.fw_config import LAUNCHPAD_LOC
from textx import metamodel_from_file
from virtmat.middleware.resconfig import ResConfig
from virtmat.language.metamodel.properties import add_properties
from virtmat.language.metamodel.processors import add_processors
from virtmat.language.utilities.textx import GRAMMAR_LOC

RESCONFIG_DICT = {
 'default_worker': {
   'name': 'test_w',
   'type_': 'SLURM',
   'queues': [{
     'name': 'test_q',
     'public': True,
     'resources': [{
       'name': 'time',
       '_maximum': 60,
       '_default': 5,
       '_fw_name': '{{virtmat.middleware.resconfig.resconfig.ResourceConfig}}'},
      {'name': 'nodes',
       '_maximum': 20,
       '_default': 1,
       '_fw_name': '{{virtmat.middleware.resconfig.resconfig.ResourceConfig}}'},
      {'name': 'cpus_per_node',
       '_maximum': 20,
       '_default': 1,
       '_fw_name': '{{virtmat.middleware.resconfig.resconfig.ResourceConfig}}'},
      {'name': 'cpus_per_task',
       '_default': 1,
       '_fw_name': '{{virtmat.middleware.resconfig.resconfig.ResourceConfig}}'},
      {'name': 'memory_per_cpu',
       '_maximum': float('inf'),
       '_default': float('inf'),
       '_fw_name': '{{virtmat.middleware.resconfig.resconfig.ResourceConfig}}'}],
     '_fw_name': '{{virtmat.middleware.resconfig.resconfig.QueueConfig}}'}],
   'accounts': ['test'],
   'modules': [{
      '_fw_name': '{{virtmat.middleware.resconfig.resconfig.ModuleConfig}}',
      'name': 'vasp',
      'path': 'null',
      'prefix': 'chem',
      'versions': ['5.4.4.pl2', '6.2.0_nvidia-21.2', '6.3.2_nvidia-21.2']
    }],
   '_default_account': 'test_g',
   '_fw_name': '{{virtmat.middleware.resconfig.resconfig.WorkerConfig}}'},
 'workers': [{
   'name': 'test_w',
   'type_': 'SLURM',
   'queues': [{
     'name': 'test_q',
     'public': True,
     'resources': [{
       'name': 'time',
       '_maximum': 60,
       '_default': 5,
       '_fw_name': '{{virtmat.middleware.resconfig.resconfig.ResourceConfig}}'},
      {'name': 'nodes',
       '_maximum': 20,
       '_default': 1,
       '_fw_name': '{{virtmat.middleware.resconfig.resconfig.ResourceConfig}}'},
      {'name': 'cpus_per_node',
       '_maximum': 20,
       '_default': 1,
       '_fw_name': '{{virtmat.middleware.resconfig.resconfig.ResourceConfig}}'},
      {'name': 'cpus_per_task',
       '_default': 1,
       '_fw_name': '{{virtmat.middleware.resconfig.resconfig.ResourceConfig}}'},
      {'name': 'memory_per_cpu',
       '_maximum': float('inf'),
       '_default': float('inf'),
       '_fw_name': '{{virtmat.middleware.resconfig.resconfig.ResourceConfig}}'}],
     '_fw_name': '{{virtmat.middleware.resconfig.resconfig.QueueConfig}}'}],
   'accounts': ['test'],
   'modules': [{
      '_fw_name': '{{virtmat.middleware.resconfig.resconfig.ModuleConfig}}',
      'name': 'vasp',
      'path': 'null',
      'prefix': 'chem',
      'versions': ['5.4.4.pl2', '6.2.0_nvidia-21.2', '6.3.2_nvidia-21.2']
    }],
   '_default_account': 'test',
   '_fw_name': '{{virtmat.middleware.resconfig.resconfig.WorkerConfig}}'}],
 '_fw_name': '{{virtmat.middleware.resconfig.resconfig.ResConfig}}'
}


@pytest.fixture(name='test_config',
                params=[(False, False), (True, False), (True, True)],
                ids=['INSTANT', 'DEFERRED', 'WORKFLOW'])
def fixture_config(request):
    """
    params is defined as (DEFERRED_MODE, WORKFLOW_MODE)
    with the 3 parameters the other fixtures and every test are parametrised
    differently and executed three times
    """
    return request.param


@pytest.fixture(name='model_kwargs')
def fixture_kwargs(test_config, lpad):
    """setting up kwargs for model instance"""
    model_kwargs = {'deferred_mode': test_config[0]}
    if test_config[1]:
        model_kwargs['model_instance'] = {'lpad': lpad, 'uuid': None}
        model_kwargs['autorun'] = True
    return model_kwargs


@pytest.fixture(name='model_kwargs_no_display')
def fixture_kwargs_no_display(model_kwargs):
    """deactivate display graphics (such as matplotlib) on the screen"""
    model_kwargs['display_graphics'] = False
    return model_kwargs


@pytest.fixture(name='raw_meta_model')
def fixture_raw_metamodel():
    """create an initial metamodel from a grammar file"""
    return metamodel_from_file(GRAMMAR_LOC, auto_init_attributes=False)


@pytest.fixture(name='meta_model')
def fixture_metamodel(test_config, raw_meta_model):
    """
    add custom properties and processors to an initial metamodel and
    return a modified metamodel
    """
    add_properties(raw_meta_model, deferred_mode=test_config[0],
                   workflow_mode=test_config[1])
    add_processors(raw_meta_model, wflow_processors=test_config[1])
    return raw_meta_model


@pytest.fixture(name='meta_model_instant')
def fixture_metamodel_instant(raw_meta_model):
    """metamodel fixture for instant evaluation mode"""
    add_properties(raw_meta_model, deferred_mode=False, workflow_mode=False)
    add_processors(raw_meta_model, wflow_processors=False)
    return raw_meta_model


@pytest.fixture(name='model_kwargs_wf')
def fixture_kwargs_wf(lpad):
    """set up kwargs for model instance for the workflow execution mode"""
    model_kwargs = {'deferred_mode': True}
    model_kwargs['model_instance'] = {'lpad': lpad, 'uuid': None}
    model_kwargs['autorun'] = True
    return model_kwargs


@pytest.fixture(name='meta_model_wf', params=[True], ids=['WORKFLOW'])
def fixture_metamodel_wf(raw_meta_model):
    """modified metamodel for the workflow execution mode"""
    add_properties(raw_meta_model, deferred_mode=True, workflow_mode=True)
    add_processors(raw_meta_model, wflow_processors=True)
    return raw_meta_model


@pytest.fixture(name='_res_config_loc')
def res_config_fixture(tmp_path, monkeypatch):
    """create a res_config file"""
    res_config_path = os.path.join(tmp_path, 'res_config.yaml')
    cfg = ResConfig.from_dict(RESCONFIG_DICT)
    cfg.default_worker = cfg.workers[0]
    cfg.default_worker.default_queue = cfg.default_worker.queues[0]
    cfg.default_worker.default_launchdir = tmp_path
    cfg.to_file(res_config_path)
    monkeypatch.setenv('RESCONFIG_LOC', res_config_path)


@pytest.fixture(name='lpad')
def lpad_fixture():
    """launchpad object as fixture for all tests"""
    return LaunchPad.from_file(LAUNCHPAD_LOC) if LAUNCHPAD_LOC else LaunchPad()
